import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  firstMonthFee: number;
  lastMonthFee: number;

  ngOnInit() {
    this.firstMonthFee = 88;
    this.lastMonthFee = 20;
  }
}
