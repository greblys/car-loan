import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { PaymentsComponent } from './payments/payments.component';
import { MatTableModule } from '@angular/material/table';
import { TopCarsComponent } from './top-cars/top-cars.component';

@NgModule({
  declarations: [
    AppComponent,
    PaymentsComponent,
    TopCarsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    MatTableModule,
    HttpClientModule,
    HttpClientJsonpModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
