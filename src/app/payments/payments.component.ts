import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { Payment } from 'Types';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnChanges {
  private monthlySum: number;
  schedule: Payment[];
  scheduleColumns: string[];

  @Input('price') price: string;
  @Input('deposit') deposit: string;
  @Input('date') date: string;
  @Input('years') years: string;
  @Input('firstMonthFee') firstMonthFee: string;
  @Input('lastMonthFee') lastMonthFee: string;
  @Output() onMonthlyChange = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    this.scheduleColumns = ['date', 'sum'];
  }

  ngOnChanges() {
    if (this.price && this.deposit && this.date && this.years) {
      this.calculateMonthlyPayment();
      this.generateSchedule();
    } else {
      this.schedule = [];
      this.monthlySum = 0;
    }
  }

  generateSchedule() {
    let schedule = [];
    let deliveryDate = new Date(this.date);
    let currentDate = new Date(this.date);
    let finalDate = new Date(this.date);
    finalDate.setFullYear(currentDate.getFullYear() + Number(this.years));

    while (currentDate < finalDate) {
      let nextMonday = this.getNextPaymentDate(currentDate);
      if (nextMonday > deliveryDate) {
        schedule.push({ date: new Date(nextMonday), sum: this.monthlySum });
      }
      nextMonday.setMonth(nextMonday.getMonth() + 1)
      currentDate = nextMonday;
    }
    schedule[0].sum += Number(this.firstMonthFee);
    schedule[schedule.length - 1].sum += Number(this.lastMonthFee);
    this.schedule = schedule;
  }

  //Traverses argument date to the next month's monday
  private getNextPaymentDate(date: Date) {
    //Traverse to next month and set day of the month to be the first.
    date = new Date(date);
    date.setDate(1);
    while (date.getDay() !== 1) {
      //traverse one day forward until Monday is reached
      date.setHours(24); 
    }
    return date;
  }

  private calculateMonthlyPayment() {
    let n = Number(this.years) * 12;
    let price = Number(this.price);
    let deposit = Number(this.deposit);
    this.monthlySum = (price - deposit) / n;
    this.onMonthlyChange.emit(this.monthlySum);
  }
}
