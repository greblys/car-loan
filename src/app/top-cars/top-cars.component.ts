import { Component, Input, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response, Car } from 'Types';

const API = 'https://www.arnoldclark.com/used-cars/search.json?payment_type=monthly&sort_order=monthly_payment_up';

@Injectable()
@Component({
  selector: 'app-top-cars',
  templateUrl: './top-cars.component.html',
  styleUrls: ['./top-cars.component.css']
})
export class TopCarsComponent {
  @Input() monthlyPrice: number;
  cars: Car[];
  columns: string[];

  constructor(private http: HttpClient) {
    
  }

  ngOnInit() {
    this.cars = [];
    this.columns = ['title', 'price', 'monthly', 'summary'];
  }

  ngOnChanges() {
    if (this.monthlyPrice) {
      this.cars = TOP_CARS.searchResults as Car[];
      this.http.jsonp(`${API}&max_price=${this.monthlyPrice}&min_price=${this.monthlyPrice - 50}`, 'ng_jsonp_callback_0')
        .subscribe((data: Response) => {
          //this.cars = data.searchResults;
        })
    } else {
      this.cars = [];
    }
  }
}

//Hardcoding 
const TOP_CARS = {
  searchResults: [
    {
    "stockReference": "ARNAM-U-63023",
      "url": "/used-cars/peugeot/108/1-0-active-3dr/2016/ref/arnam-u-63023",
        "enquiryUrl": "/contact-us/vehicle-enquiry/ARNAM-U-63023",
          "isReserved": false,
            "isAvailableSoon": false,
              "isAwaitingImages": false,
                "title": {
      "name": "2016 Peugeot 108",
        "variant": "1.0 Active 3dr"
    },
    "photos": [],
      "thumbnails": [],
        "imageCount": 0,
          "branch": {
      "name": "Newcastle Ford",
        "url": "/find-a-dealer/newcastle-ford/ref/arnam",
          "distance": null,
            "merchantId": "303172",
              "providerId": "15286",
                "address": "Scotswood Road, Newcastle, NE4 7BP",
                  "phone": "0191 644 0711",
                    "id": "arnam",
                      "firstImage": "//ac-branches.s3.amazonaws.com/branches/newcastle-ford_7db71a099d2a8e3be5dea4d927725b03b5431151.jpg"
    },
    "make": "Peugeot",
      "model": "108",
        "citnowVideo": null,
          "salesInfo": {
      "pricing": {
        "cashPricePrefix": "Expected retail price",
          "cashPrice": 4998,
            "monthlyPayment": 85.03,
              "deposit": 300,
                "financeHeading": "Representative finance example"
      },
      "summary": [
        "26,893 miles",
        "Used",
        "68.9 MPG*",
        "Hatchback",
        "Road tax free",
        "Petrol",
        "Black"
      ],
        "highlightedFeature": null
    }
  },
  {
    "stockReference": "ARNAK-U-580687",
      "url": "/used-cars/dacia/sandero/1-0-sce-access-5dr/2017/ref/arnak-u-580687",
        "enquiryUrl": "/contact-us/vehicle-enquiry/ARNAK-U-580687",
          "isReserved": false,
            "isAvailableSoon": false,
              "isAwaitingImages": false,
                "title": {
      "name": "2017 (67) Dacia Sandero",
        "variant": "1.0 SCe Access 5dr"
    },
    "photos": [],
      "thumbnails": [],
        "imageCount": 0,
          "branch": {
      "name": "Glasgow London Road Motorstore",
        "url": "/find-a-dealer/glasgow-london-road-motorstore/ref/arnak",
          "distance": null,
            "merchantId": "306477",
              "providerId": "15303",
                "address": "15 Westhorn Drive, Glasgow, G32 8YX",
                  "phone": "0141 278 7552",
                    "id": "arnak",
                      "firstImage": "//ac-branches.s3.amazonaws.com/branches/glasgow-london-road-motorstore_1f84c078076974ba7194aa4aaae723a1fd98467b.jpg"
    },
    "make": "Dacia",
      "model": "Sandero",
        "citnowVideo": null,
          "salesInfo": {
      "pricing": {
        "cashPricePrefix": "Expected retail price",
          "cashPrice": 4998,
            "monthlyPayment": 91.35,
              "deposit": 300,
                "financeHeading": "Representative finance example"
      },
      "summary": [
        "25,318 miles",
        "Used",
        "54.3 MPG*",
        "Hatchback",
        "Petrol",
        "White"
      ],
        "highlightedFeature": null
    }
  },
  {
    "stockReference": "ARNCZ-U-27885",
      "url": "/used-cars/dacia/sandero/1-5-dci-laureate-prime-5dr/2015/ref/arncz-u-27885",
        "enquiryUrl": "/contact-us/vehicle-enquiry/ARNCZ-U-27885",
          "isReserved": true,
            "isAvailableSoon": false,
              "isAwaitingImages": false,
                "title": {
      "name": "2015 (15) Dacia Sandero",
        "variant": "1.5 dCi Laureate Prime 5dr"
    },
    "photos": [
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/f",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/r",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/i",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/4",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/5",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/6",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/7",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/8",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/9",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/10",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/11",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/12",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/13",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/14",
      "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/800/15"
    ],
      "thumbnails": [
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/f",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/r",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/i",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/4",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/5",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/6",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/7",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/8",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/9",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/10",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/11",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/12",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/13",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/14",
        "https://vcache.arnoldclark.com/imageserver/ASRHNHC5Z1-EUS2/350/15"
      ],
        "imageCount": 15,
          "branch": {
      "name": "Bishopbriggs Renault / Dacia",
        "url": "/find-a-dealer/bishopbriggs-renault-dacia/ref/arncz",
          "distance": null,
            "merchantId": "304474",
              "providerId": "15583",
                "address": "64 Kirkintilloch Rd, Bishopbriggs, G64 2AH",
                  "phone": "0141 305 9405",
                    "id": "arncz",
                      "firstImage": "//ac-branches.s3.amazonaws.com/branches/bishopbriggs-renault-dacia_3dda863bcd1dce88c7ed3eddda5f80e392293c8c.jpg"
    },
    "make": "Dacia",
      "model": "Sandero",
        "citnowVideo": "https://www.arnoldclarkcarpreview.com/embed/K2Qtjf_6G?vid=h5rnml&autostart=true",
          "salesInfo": {
      "pricing": {
        "cashPricePrefix": "Only",
          "cashPrice": 4998
      },
      "summary": [
        "61,473 miles",
        "Used",
        "74.3 MPG*",
        "Hatchback",
        "Road tax free",
        "Diesel",
        "Blue"
      ],
        "highlightedFeature": null
    }
  },
  {
    "stockReference": "ARNFB-U-29234",
      "url": "/used-cars/ford/ka/1-2-metal-3dr-start-stop/2015/ref/arnfb-u-29234",
        "enquiryUrl": "/contact-us/vehicle-enquiry/ARNFB-U-29234",
          "isReserved": false,
            "isAvailableSoon": false,
              "isAwaitingImages": false,
                "title": {
      "name": "2015 (15) Ford Ka",
        "variant": "1.2 Metal 3dr [Start Stop]"
    },
    "photos": [],
      "thumbnails": [],
        "imageCount": 0,
          "branch": {
      "name": "Stoke Vauxhall",
        "url": "/find-a-dealer/stoke-vauxhall/ref/arnfb",
          "distance": null,
            "merchantId": "327467",
              "providerId": "15567",
                "address": "Clough Street, Stoke on Trent, ST1 4AS",
                  "phone": "01782 580898",
                    "id": "arnfb",
                      "firstImage": "//ac-branches.s3.amazonaws.com/branches/stoke-vauxhall_92be073f4fe6e55dbe6afdec02f9ed9f7313cd56.jpg"
    },
    "make": "Ford",
      "model": "Ka",
        "citnowVideo": null,
          "salesInfo": {
      "pricing": {
        "cashPricePrefix": "Expected retail price",
          "cashPrice": 4998,
            "monthlyPayment": 116.9,
              "deposit": 300,
                "financeHeading": "Representative finance example"
      },
      "summary": [
        "24,250 miles",
        "Used",
        "57.7 MPG*",
        "Hatchback",
        "Road tax £30",
        "Petrol",
        "Black"
      ],
        "highlightedFeature": null
    }
  },
  {
    "stockReference": "cc_dbkyqh3a6vil5qi6",
      "url": "/used-cars/ford/focus/1-6-tdci-115-edge-5dr/2014/ref/cc_dbkyqh3a6vil5qi6",
        "enquiryUrl": "/contact-us/vehicle-enquiry/cc_dbkyqh3a6vil5qi6",
          "isReserved": false,
            "isAvailableSoon": false,
              "isAwaitingImages": true,
                "title": {
      "name": "2014 (64) Ford Focus",
        "variant": "1.6 TDCi 115 Edge 5dr"
    },
    "photos": [
      "https://vcache.arnoldclark.com/imageserver/COCW_CD4B6KLYFH/800/f"
    ],
      "thumbnails": [
        "https://vcache.arnoldclark.com/imageserver/COCW_CD4B6KLYFH/350/f"
      ],
        "imageCount": 1,
          "branch": {
      "name": "Paisley Fiat / Abarth",
        "url": "/find-a-dealer/paisley-fiat-abarth/ref/arneg",
          "distance": null,
            "merchantId": "302346",
              "providerId": "15281",
                "address": "Renfrew Road, Paisley, PA3 4AR",
                  "phone": "0141 308 1972",
                    "id": "arneg",
                      "firstImage": "//ac-branches.s3.amazonaws.com/branches/paisley-fiat-abarth_20ffeac02619b6c7af427d9c03783f75fa23e9fa.jpg"
    },
    "make": "Ford",
      "model": "Focus",
        "citnowVideo": null,
          "salesInfo": {
      "pricing": {
        "cashPricePrefix": "Expected retail price",
          "cashPrice": 4998,
            "monthlyPayment": 100.69,
              "deposit": 100,
                "financeHeading": "Representative finance example"
      },
      "summary": [
        "72,173 miles",
        "Used",
        "67.3 MPG*",
        "Hatchback",
        "Road tax £20",
        "Diesel",
        "Red"
      ],
        "highlightedFeature": null
    }
  },
  {
    "stockReference": "ARNBS-U-525397",
      "url": "/used-cars/fiat/punto/1-4-easy-3dr/2015/ref/arnbs-u-525397",
        "enquiryUrl": "/contact-us/vehicle-enquiry/ARNBS-U-525397",
          "isReserved": false,
            "isAvailableSoon": false,
              "isAwaitingImages": true,
                "title": {
      "name": "2015 (15) Fiat Punto",
        "variant": "1.4 Easy 3dr"
    },
    "photos": [
      "https://vcache.arnoldclark.com/imageserver/AORWNPB5S1-DUS5/800/f"
    ],
      "thumbnails": [
        "https://vcache.arnoldclark.com/imageserver/AORWNPB5S1-DUS5/350/f"
      ],
        "imageCount": 1,
          "branch": {
      "name": "Linwood Citroën",
        "url": "/find-a-dealer/linwood-citroen/ref/arnbs",
          "distance": null,
            "merchantId": "326575",
              "providerId": "15599",
                "address": "Phoenix Retail Park, Linwood, PA1 2BH",
                  "phone": "0141 278 7747",
                    "id": "arnbs",
                      "firstImage": "//ac-branches.s3.amazonaws.com/branches/linwood-citroen_98be181b6413911fa0d1165b5e76ba8d3c37047c.jpg"
    },
    "make": "Fiat",
      "model": "Punto",
        "citnowVideo": "https://www.arnoldclarkcarpreview.com/embed/X5SD4xDw5?vid=h5rnml&autostart=true",
          "salesInfo": {
      "pricing": {
        "cashPricePrefix": "Expected retail price",
          "cashPrice": 4998,
            "monthlyPayment": 122.09,
              "deposit": 300,
                "financeHeading": "Representative finance example"
      },
      "summary": [
        "10,231 miles",
        "Used",
        "49.6 MPG*",
        "Hatchback",
        "Road tax £140",
        "Petrol",
        "Orange"
      ],
        "highlightedFeature": null
    }
    }
  ]
}
