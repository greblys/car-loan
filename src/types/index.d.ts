declare module "Types" {
  export type Payment = {
    date: Date;
    sum: number;
  }

  export type Response = {
    searchResults: Car[];
  }

  export type Car = {
    title: { name: string, variant: string },
    make: string,
    model: string,
    url: string,
    salesInfo: {
      pricing: {
        cashPricePrefix: string,
        cashPrice: number,
        monthlyPayment: number,
        deposit: number,
        financeHeading: string
      },
      summary: string []
    }
  }
}
